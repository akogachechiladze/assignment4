package com.example.usaidshemajamebeli4tictactoe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.usaidshemajamebeli4tictactoe.databinding.FragmentFourXFourBinding
import com.example.usaidshemajamebeli4tictactoe.databinding.FragmentThreeXThreeBinding
import kotlinx.android.synthetic.main.fragment_four_x_four.*
import kotlinx.android.synthetic.main.fragment_three_x_three.*
import kotlinx.android.synthetic.main.fragment_three_x_three.button00
import kotlinx.android.synthetic.main.fragment_three_x_three.button01
import kotlinx.android.synthetic.main.fragment_three_x_three.button02
import kotlinx.android.synthetic.main.fragment_three_x_three.button10
import kotlinx.android.synthetic.main.fragment_three_x_three.button11
import kotlinx.android.synthetic.main.fragment_three_x_three.button12
import kotlinx.android.synthetic.main.fragment_three_x_three.button20
import kotlinx.android.synthetic.main.fragment_three_x_three.button21
import kotlinx.android.synthetic.main.fragment_three_x_three.button22
import kotlinx.android.synthetic.main.fragment_three_x_three.restartbutton


class FourXFourFragment : Fragment(), View.OnClickListener {

    private var binding: FragmentFourXFourBinding? = null
    private var isPlayerOne = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFourXFourBinding.inflate(inflater, container, false)
        init()
        return binding?.root
    }

    private fun init(){
        binding?.button00?.setOnClickListener(this)
        binding?.button01?.setOnClickListener(this)
        binding?.button02?.setOnClickListener(this)
        binding?.button03?.setOnClickListener(this)
        binding?.button10?.setOnClickListener(this)
        binding?.button11?.setOnClickListener(this)
        binding?.button12?.setOnClickListener(this)
        binding?.button13?.setOnClickListener(this)
        binding?.button20?.setOnClickListener(this)
        binding?.button21?.setOnClickListener(this)
        binding?.button22?.setOnClickListener(this)
        binding?.button23?.setOnClickListener(this)
        binding?.button30?.setOnClickListener(this)
        binding?.button31?.setOnClickListener(this)
        binding?.button32?.setOnClickListener(this)
        binding?.button33?.setOnClickListener(this)
        restartGame()

    }

    override fun onClick(v: View?) {
        buttonListenerText(v as Button)
    }


    private fun buttonListenerText(button: Button){

        if (isPlayerOne){
            button.text = "x"
        }else {
            button.text = "O"
        }
        isPlayerOne = !isPlayerOne
        button.isClickable = false
        checkWinner()
        restartGame()
    }

    private fun checkWinner(){
        if (binding?.button00?.text!!.isNotEmpty() && binding?.button00?.text.toString() == binding?.button01?.text.toString() && binding?.button02?.text.toString()==binding?.button01?.text.toString() && binding?.button02?.text.toString()==binding?.button03?.text.toString()){
            showToast(binding?.button00?.text.toString())
            isClickable()
        }else if (binding?.button10?.text!!.isNotEmpty() && binding?.button10?.text.toString() == binding?.button11?.text.toString() && binding?.button12?.text.toString()== binding?.button11?.text.toString() && binding?.button12?.text.toString()== binding?.button13?.text.toString()) {
            showToast(binding?.button10?.text.toString())
            isClickable()
        }else if (binding?.button20?.text!!.isNotEmpty() && binding?.button20?.text.toString() == binding?.button21?.text.toString() && binding?.button22?.text.toString() == binding?.button21?.text.toString()  && binding?.button22?.text.toString() == binding?.button23?.text.toString()) {
            showToast(binding?.button20?.text.toString())
            isClickable()
        }else if (binding?.button30?.text!!.isNotEmpty() && binding?.button30?.text.toString() == binding?.button31?.text.toString() && binding?.button32?.text.toString() == binding?.button31?.text.toString()  && binding?.button32?.text.toString() == binding?.button33?.text.toString()) {
            showToast(binding?.button20?.text.toString())
            isClickable()
        }else if (binding?.button00?.text!!.isNotEmpty() && binding?.button00?.text.toString() == binding?.button11?.text.toString() && binding?.button22?.text.toString() == binding?.button11?.text.toString() && binding?.button22?.text.toString() == binding?.button33?.text.toString()) {
            showToast(binding?.button00?.text.toString())
            isClickable()
        }else if (binding?.button30?.text!!.isNotEmpty() && binding?.button30?.text.toString() == binding?.button21?.text.toString() && binding?.button21?.text.toString() == binding?.button12?.text.toString() && binding?.button12?.text.toString() == binding?.button03?.text.toString()) {
            showToast(binding?.button20?.text.toString())
            isClickable()
        }else if (binding?.button00?.text!!.isNotEmpty() && binding?.button00?.text.toString() == binding?.button10?.text.toString() && binding?.button10?.text.toString() == binding?.button20?.text.toString() && binding?.button10?.text.toString() == binding?.button30?.text.toString()) {
            showToast(binding?.button10?.text.toString())
            isClickable()
        }else if (binding?.button01?.text!!.isNotEmpty() && binding?.button01?.text.toString() == binding?.button11?.text.toString() && binding?.button11?.text.toString() == binding?.button12?.text.toString()  && binding?.button11?.text.toString() == binding?.button31?.text.toString()) {
            showToast(binding?.button10?.text.toString())
            isClickable()
        }else if (binding?.button02?.text!!.isNotEmpty() && binding?.button02?.text.toString() == binding?.button12?.text.toString() && binding?.button12?.text.toString() == binding?.button22?.text.toString()  && binding?.button22?.text.toString() == binding?.button32?.text.toString()) {
            showToast(binding?.button10?.text.toString())
            isClickable()
        }else if (binding?.button03?.text!!.isNotEmpty() && binding?.button03?.text.toString() == binding?.button13?.text.toString() && binding?.button13?.text.toString() == binding?.button23?.text.toString()  && binding?.button23?.text.toString() == binding?.button33?.text.toString()) {
            showToast(binding?.button10?.text.toString())
            isClickable()
        }else if (binding?.button00?.text!!.isNotEmpty() && binding?.button01?.text!!.isNotEmpty() && binding?.button02?.text!!.isNotEmpty() && binding?.button03!!.text.isNotEmpty() && binding?.button10?.text!!.isNotEmpty() && binding?.button11?.text!!.isNotEmpty() && binding?.button12?.text!!.isNotEmpty() && binding?.button13!!.text.isNotEmpty() && binding?.button20?.text!!.isNotEmpty() && binding?.button21?.text!!.isNotEmpty() && binding?.button22?.text!!.isNotEmpty() && binding?.button23!!.text.isNotEmpty() && binding?.button30?.text!!.isNotEmpty() && binding?.button31?.text!!.isNotEmpty() && binding?.button32?.text!!.isNotEmpty() && binding?.button33?.text!!.isNotEmpty()){
            Toast.makeText(requireContext(),"Tie", Toast.LENGTH_SHORT).show()
        }



        restartGame()
    }

    private fun showToast(message: String) =
        Toast.makeText(requireContext(),"Winner is $message", Toast.LENGTH_SHORT).show()

    private fun isClickable(){
        binding?.button00?.isClickable = false
        binding?.button01?.isClickable = false
        binding?.button02?.isClickable = false
        binding?.button03?.isClickable = false
        binding?.button10?.isClickable = false
        binding?.button11?.isClickable = false
        binding?.button12?.isClickable = false
        binding?.button13?.isClickable = false
        binding?.button20?.isClickable = false
        binding?.button21?.isClickable = false
        binding?.button22?.isClickable = false
        binding?.button23?.isClickable = false
        binding?.button30?.isClickable = false
        binding?.button31?.isClickable = false
        binding?.button32?.isClickable = false
        binding?.button33?.isClickable = false
        restartGame()
    }

    private fun restartGame(){
        binding?.restartbutton?.setOnClickListener {
            binding?.button00?.setText("")
            binding?.button01?.setText("")
            binding?.button02?.setText("")
            binding?.button03?.setText("")
            binding?.button10?.setText("")
            binding?.button11?.setText("")
            binding?.button12?.setText("")
            binding?.button13?.setText("")
            binding?.button20?.setText("")
            binding?.button21?.setText("")
            binding?.button22?.setText("")
            binding?.button23?.setText("")
            binding?.button30?.setText("")
            binding?.button31?.setText("")
            binding?.button32?.setText("")
            binding?.button33?.setText("")
            binding?.button00?.isClickable = true
            binding?.button01?.isClickable = true
            binding?.button02?.isClickable = true
            binding?.button03?.isClickable = true
            binding?.button10?.isClickable = true
            binding?.button11?.isClickable = true
            binding?.button12?.isClickable = true
            binding?.button13?.isClickable = true
            binding?.button20?.isClickable = true
            binding?.button21?.isClickable = true
            binding?.button22?.isClickable = true
            binding?.button23?.isClickable = true
            binding?.button30?.isClickable = true
            binding?.button31?.isClickable = true
            binding?.button32?.isClickable = true
            binding?.button33?.isClickable = true
            isPlayerOne = true

        }
    }

}
