package com.example.usaidshemajamebeli4tictactoe

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.example.usaidshemajamebeli4tictactoe.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment() {

    private var binding: FragmentRegistrationBinding? = null

    var three = false
    var four = false
    var five = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegistrationBinding.inflate(inflater, container, false)


        init()

        return binding?.root

    }

    private fun init() {

        binding?.threeXthree?.setOnClickListener {
            binding?.threeXthree?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.positive_butotn))
            binding?.fourXfour?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            binding?.fiveXfive?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            three = true
            four = false
            five = false
        }

        binding?.fourXfour?.setOnClickListener {
            binding?.threeXthree?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            binding?.fourXfour?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.positive_butotn))
            binding?.fiveXfive?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            three = false
            four = true
            five = false
        }

        binding?.fiveXfive?.setOnClickListener {
            binding?.threeXthree?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            binding?.fourXfour?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.negative_button))
            binding?.fiveXfive?.setBackground(ContextCompat.getDrawable(requireActivity(), R.drawable.positive_butotn))
            three = false
            four = false
            five = true
        }

        binding?.Startgame?.setOnClickListener {
            startGame()
        }



    }

    private fun startGame() {
        if (three == true) {
            binding?.Startgame?.setOnClickListener {
                findNavController().navigate(R.id.action_registrationFragment2_to_threeXThreeFragment3)
            }
        }else if (four == true) {
            binding?.Startgame?.setOnClickListener {
                findNavController().navigate(R.id.action_registrationFragment2_to_fourXFourFragment)
            }
        }else if (five == true) {
            binding?.Startgame?.setOnClickListener {
                findNavController().navigate(R.id.action_registrationFragment2_to_fiveXFiveFragment)
            }
        }else if (three == false && four == false && five == false) {
            Toast.makeText(requireContext(), "Please choose game type", Toast.LENGTH_SHORT).show()
        }

    }



}